#
#  Be sure to run `pod spec lint ProgressHUD.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "ProgressHUD"
  s.version      = "1.0"
  s.summary      = "Simple ProgressHUD in center of the screen."
  s.homepage     = "https://bitbucket.org/HarhunTsimafei/dikidiprogresshud"
  s.license      = "MIT"
  s.author       = { "Tsimafei Harhun" => "timgorgun@gmail.com" }
  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"
  s.source       = { :git => "https://bitbucket.org/HarhunTsimafei/dikidiprogresshud.git"}

  s.source_files  = "ProgressHUD", "ProgressHUD/**/*.{h,m,swift}"
  s.exclude_files = "Classes/Exclude"
 end
