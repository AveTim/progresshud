//
//  HUDContainerView.swift
//  ProgressHUD
//
//  Created by Brothers Harhun on 2/21/18.
//  Copyright © 2018 Brothers Harhun. All rights reserved.
//

import Foundation
import UIKit


public struct HUDOptions {
    public static var padding : CGFloat = 22
}


class HUDContainerView: UIView {
    
    let backgroundView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var allOutlets: [UIView] {
        return []
    }
    
    fileprivate func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(backgroundView)

        for childView in allOutlets {
            addSubview(childView)
            childView.translatesAutoresizingMaskIntoConstraints = false
        }
        installConstaints()
        setupAttrs()
    }
    
    func installConstaints() {}
    
    func setupAttrs() {}
}


class HUDIndicatorContainerView: HUDContainerView {
    let indicator:UIView
    
    init(indicator:UIView) {
        self.indicator = indicator
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience override init() {
        self.init(indicator: UIActivityIndicatorView(activityIndicatorStyle: .white))
    }
    
    override var allOutlets: [UIView] {
        return [indicator]
    }
    
    override func installConstaints() {
        super.installConstaints()
        
        let padding = HUDOptions.padding
        indicator.pa_top.eq(padding).install()
        indicator.pa_centerX.install()
        indicator.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
    }
    
    override var intrinsicContentSize: CGSize {
        let padding = HUDOptions.padding
        let indicatorSize = indicator.intrinsicContentSize
        
        let width = indicatorSize.width + padding * 2
        let height = indicatorSize.height + padding * 2
        return CGSize(width: width, height: height)
    }

}
