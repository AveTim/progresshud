//
//  HUD.swift
//  ProgressHUD
//
//  Created by Brothers Harhun on 2/21/18.
//  Copyright © 2018 Brothers Harhun. All rights reserved.
//

import UIKit


public struct HUD {

    fileprivate static var window: UIWindow? {
        if let win =  UIApplication.shared.delegate?.window {
            return win
        }
        return nil
    }
    
    public static func showProgress() {
        let hud = globalHUD()
        guard let window = window else { return }
        window.bringSubview(toFront: hud)
        hud.show(false)
    }
    
    public static func hideProgress() {
        guard let window = window else { return }
        let hud = globalHUD()
        window.bringSubview(toFront: hud)
        hud.hide(afterDelay: 0.01)
    }
    
    private static func globalHUD() -> ProgressHUD {
        guard let window = window else{ return ProgressHUD() }
        let hud: ProgressHUD
        if let existsHUD = ProgressHUD.HUDForView(window) {
            hud = existsHUD
        } else {
            hud = ProgressHUD.Builder(forView: window).create()
        }
        return hud
    }

}
