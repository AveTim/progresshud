//
//  ProgressHUD.swift
//  ProgressHUD
//
//  Created by Brothers Harhun on 2/21/18.
//  Copyright © 2018 Brothers Harhun. All rights reserved.
//

import UIKit


open class ProgressHUD: UIView {
    
    open var blurStyle: UIBlurEffectStyle = .dark {
        didSet {
            containerView.backgroundView.effect = UIBlurEffect(style: blurStyle)
        }
    }
    
    open var activityIndicatorColor: UIColor = .white {
        didSet {
            if let activityIndicator = indicator as? UIActivityIndicatorView {
                activityIndicator.color = activityIndicatorColor
            }
        }
    }
    
    fileprivate var indicator: UIView? {
        if let indicatorContainer = containerView as? HUDIndicatorContainerView {
            return indicatorContainer.indicator
        }
        return nil
    }
    
    fileprivate var useAnimation = false
    
    fileprivate var showStarted: Date?
    
    fileprivate var containerView: HUDContainerView = HUDIndicatorContainerView()
    
    public init() {
        super.init(frame: CGRect.zero)
        self.shouldUpdateIndicators()
        self.containerView = self.createContainerView()
        commonInit()
    }
    
    open func attachTo(_ view: UIView) {
        self.removeFromSuperview()
        frame = view.bounds
        view.addSubview(self)
    }
    
    @nonobjc
    open func attachTo(_ window: UIWindow) {
        self.attachTo(window as UIView)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        backgroundColor = .clear
        isOpaque = false
        alpha = 0.0
        shouldUpdateIndicators()
    }
    
    func createIndicator() -> UIView? {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.color = activityIndicatorColor
        indicator.startAnimating()
        return indicator
    }
    
    func createContainerView() -> HUDContainerView {
        guard let indicatior = createIndicator() else { return HUDContainerView() }
        return HUDIndicatorContainerView(indicator: indicatior )
    }
    
    func shouldUpdateIndicators() {
        containerView.removeFromSuperview()
        containerView = createContainerView()
        
        containerView.backgroundView.effect = UIBlurEffect(style: blurStyle)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let blurView = containerView.backgroundView
        blurView.layer.cornerRadius = 10
        blurView.clipsToBounds = true
        
        addSubview(containerView)
        installConstraints()
    }
    
    func installConstraints() {
        containerView.pa_centerX.install()
        containerView.pa_centerY.install()
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
}


extension ProgressHUD {
    public static func HUDForView(_ view: UIView) -> ProgressHUD? {
        return view.subviews.reversed().filter { $0 is ProgressHUD }.first as? ProgressHUD
    }
}

extension ProgressHUD {
    public func showAnimated(_ animated: Bool,
                             whileExecutingBlock block: @escaping ()->(),
                             onQueue queue:DispatchQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)){
        queue.async {
            block()
            DispatchQueue.main.async {
                self.hide(animated)
            }
        }
        self.show(animated)
    }
    
    func delay(_ delay:TimeInterval,closure:@escaping ()-> Void) {
        let when = DispatchTime.now() + Double(Int64(delay * TimeInterval(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: when , execute: closure)
    }
}

extension ProgressHUD {
    public func show(_ animated: Bool = true) {
        useAnimation = animated
        self.showUsingAnimation(self.useAnimation)
    }
    
    public func hide(_ animated: Bool = true) {
        useAnimation = animated
        self.hideUsingAnimation(self.useAnimation)
        return
    }
    
    public func hide(_ animated: Bool = true, afterDelay seconds: TimeInterval) {
        delay(seconds) {
            self.hide(animated)
        }
    }
    
    private func showUsingAnimation(_ animated: Bool) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        setNeedsDisplay()
        showStarted = Date()
        
        if animated {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.alpha = 1.0
            self.transform = CGAffineTransform.identity
            UIView.commitAnimations()
        } else {
            self.alpha = 1.0
        }
    }
    
    private func hideUsingAnimation(_ animated: Bool) {
        if animated && showStarted != nil {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0.02
            }, completion: { (finished) -> Void in
                self.done()
            })
        } else {
            done()
        }
        showStarted = nil
    }
    
    func done() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        alpha = 0.0
        setNeedsDisplay()
    }
}


// MARK: - Builder
public extension ProgressHUD {
    public class Builder {
        let hud: ProgressHUD
        let targetView: UIView
        
        public init(forView: UIView) {
            hud = ProgressHUD()
            targetView = forView
        }
        
        public convenience init() {
            let window = UIApplication.shared.keyWindow!
            self.init(forView:window)
        }
        
        open func show(_ animated: Bool = true) -> ProgressHUD {
            create().show(animated)
            return hud
        }
        
        open func create() -> ProgressHUD {
            hud.attachTo(targetView)
            return hud
        }
    }
}
