//
//  UIView+Helper.swift
//  ProgressHUD
//
//  Created by Brothers Harhun on 2/21/18.
//  Copyright © 2018 Brothers Harhun. All rights reserved.
//

import UIKit

extension UIView {
    
    internal func assertHasSuperview(){
        assert(superview != nil, "NO SUPERVIEW")
    }
    
    internal func assertIsSibling(_ sibling:UIView){
        assert(superview != nil, "NO SUPERVIEW")
        assert(superview == sibling.superview, "NOT SIBLING")
    }

}

