//
//  UIView+PinAuto.swift
//  ProgressHUD
//
//  Created by Brothers Harhun on 2/21/18.
//  Copyright © 2018 Brothers Harhun. All rights reserved.
//

import UIKit


open class LayoutConstraintParams {
    open var priority: UILayoutPriority = UILayoutPriority.required
    open var relation:  NSLayoutRelation = NSLayoutRelation.equal
    open var firstItemAttribute: NSLayoutAttribute = NSLayoutAttribute.notAnAttribute
    open var secondItemAttribute: NSLayoutAttribute = NSLayoutAttribute.notAnAttribute
    open var multiplier: CGFloat = 1.0
    open var constant: CGFloat  = 0
    open let firstItem: UIView
    open var secondItem: AnyObject?
    open var identifier: String? = LayoutConstraintParams.constraintIdentifier

    open static let constraintIdentifier = "pin_auto"
    fileprivate let attributesOfOpposite: [NSLayoutAttribute] = [.right,.rightMargin,.trailing,.trailingMargin,.bottom,.bottomMargin]

    fileprivate var shouldReverseValue: Bool{
        if firstItemAttribute == secondItemAttribute{
            return attributesOfOpposite.contains(firstItemAttribute)
        }
        return false
    }

    public init(firstItem:UIView){
        self.firstItem = firstItem
    }

    open func eq(_ value:CGFloat) -> LayoutConstraintParams{
        constant = value
        relation = .equal
        return self
    }
    
    open func identifier(_ id:String?) -> LayoutConstraintParams{
        self.identifier = id
        return self
    }

    @discardableResult
    open func install() -> NSLayoutConstraint{
        let finalConstanValue  = shouldReverseValue ? -constant : constant
        let constraint = NSLayoutConstraint(item: firstItem,
                                            attribute: firstItemAttribute,
                                            relatedBy: relation,
                                            toItem: secondItem,
                                            attribute: secondItemAttribute,
                                            multiplier: multiplier,
                                            constant: finalConstanValue)
        constraint.identifier = identifier
        firstItem.translatesAutoresizingMaskIntoConstraints = false

        if let secondItem = secondItem{
            firstItem.assertHasSuperview()
            let containerView:UIView
            if let secondItemView = secondItem as? UIView{
                if firstItem.superview == secondItemView{
                    containerView = secondItemView
                }else if firstItem.superview == secondItemView.superview{
                    containerView = firstItem.superview!
                }else{
                    fatalError("Second Item Should be First Item 's superview or sibling view")
                }
            }else if secondItem is UILayoutSupport{
                containerView = firstItem.superview!
            }else{
                fatalError("Second Item Should be UIView or UILayoutSupport")
            }
            containerView.addConstraint(constraint)
        }else{
            firstItem.addConstraint(constraint)
        }

        return constraint
    }

}


// PinAuto Core Method
public extension UIView{
    
    fileprivate var pa_makeConstraint:LayoutConstraintParams{
        assertHasSuperview()
        return LayoutConstraintParams(firstItem: self)
    }

    public var pa_top: LayoutConstraintParams{
        let pa = pa_makeConstraint
        pa.secondItem = superview
        pa.firstItemAttribute = .top
        pa.secondItemAttribute = .top
        return pa
    }
    
    public var pa_centerX: LayoutConstraintParams{
        let pa = pa_makeConstraint
        pa.secondItem = superview
        pa.firstItemAttribute = .centerX
        pa.secondItemAttribute = .centerX
        return pa
    }
    
    public var pa_centerY: LayoutConstraintParams{
        let pa = pa_makeConstraint
        pa.secondItem = superview
        pa.firstItemAttribute = .centerY
        pa.secondItemAttribute = .centerY
        return pa
    }
 
}
